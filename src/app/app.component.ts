import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'react-in-angular';

  public counter = 21;

  public handleOnClick(stateCounter: number) {
    this.counter++;
  }
}
